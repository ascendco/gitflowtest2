import React from 'react';
import { shallow } from 'enzyme';
import { DefaultPage } from '../../../src/features/washer-list/DefaultPage';

describe('washer-list/DefaultPage', () => {
  it('renders node with correct class name', () => {
    const props = {
      washerList: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <DefaultPage {...props} />
    );

    expect(
      renderedComponent.find('.washer-list-default-page').length
    ).toBe(1);
  });
});
