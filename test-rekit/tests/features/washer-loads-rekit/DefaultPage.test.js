import React from 'react';
import { shallow } from 'enzyme';
import { DefaultPage } from '../../../src/features/washer-loads-rekit/DefaultPage';

describe('washer-loads-rekit/DefaultPage', () => {
  it('renders node with correct class name', () => {
    const props = {
      washerLoadsRekit: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <DefaultPage {...props} />
    );

    expect(
      renderedComponent.find('.washer-loads-rekit-default-page').length
    ).toBe(1);
  });
});
