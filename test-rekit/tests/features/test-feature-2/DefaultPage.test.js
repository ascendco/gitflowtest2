import React from 'react';
import { shallow } from 'enzyme';
import { DefaultPage } from '../../../src/features/test-feature-2/DefaultPage';

describe('test-feature-2/DefaultPage', () => {
  it('renders node with correct class name', () => {
    const props = {
      testFeature2: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <DefaultPage {...props} />
    );

    expect(
      renderedComponent.find('.test-feature-2-default-page').length
    ).toBe(1);
  });
});
